 #pragma once 

// Configure
#ifndef I2C_LCD_CONFIG
#define I2C_LCD_CONFIG
#define I2C_LCD_CONFIG_ENABLE_PRINTF
//#define I2C_LCD_CONFIG_ENABLE_CONTRAST
#endif

#include <mbed.h>

class LCD {
public:
    LCD(PinName sda, PinName scl);
    LCD(I2C& i2c);
    void reset();
    void clear();
    LCD locate(int c, int r);
    LCD home();
    void put(char ch);
    void puts(const char *str);
#ifdef I2C_LCD_CONFIG_ENABLE_PRINTF
    void printf(const char *fmt, ...);
#endif
#ifdef I2C_LCD_CONFIG_ENABLE_CONTRAST
    void contrast(uint8_t contrast);
    void resetContrast();
#endif

private:
    I2C _i2c;

    void write(char data);
    void write_cmd(char data);
};
