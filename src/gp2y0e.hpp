//2016/09/17

#include <mbed.h>

#ifndef GP2Y0E_I2C_INCLUDE
#define GP2Y0E_I2C_INCLUDE

//Config
//!Enable to Address Write.
#define GP2Y0E_I2C_ADDR_WRITE
//Config_end

class GP2Y_I2C {
private:
  I2C _i2c;
  DigitalOut _vpp;

  char Distance_addr[2];
  char ShiftBit_addr;
  char dateaddr[2];
  char cmd[2];
  char CatchShiftBit;

  void Init(void)
  {
    _vpp = 0;
    Distance_addr[0] = 0x5E;
    Distance_addr[1] = 0x5F;
    ShiftBit_addr = 0x35;
    CatchShiftBit = 0;
  }
  void i2c_write(int addr,char regist,char data);
  char i2c_read(int addr,char regist);

public:

 GP2Y_I2C(PinName sda, PinName scl, PinName Gvpp) : _i2c(sda, scl), _vpp(Gvpp){
   Init();
 }

 GP2Y_I2C(I2C& i2c, PinName Gvpp) : _i2c(i2c), _vpp(Gvpp){
   Init();
 }

  /* Search Distance */
  int Distance(int addr);

  /* Check Shift Bit (0x01 ... Maximum Display 128cm , 0x02 ... Maximum Display 64cm(Default)) */
  int ShiftBit_Check(int addr);

  /*Address Write*/
  #ifdef GP2Y0E_I2C_ADDR_WRITE
  void GP2Y_ADDR_Write_Sequence(char SelectAd);
  #endif
};

#endif
