#include "mbed.h"
#include "i2clcd.hpp"
#include "gp2y0e.hpp"

//Mode Settings (Choice ...1 else ...0)
#define MenuEnable  (1)
#define WriteEnable (0)
#define TestEnable  (0)

//#define AllTestMode //All Address test(ad:0x00 - 0xf0. Distance). Using Conputer on Console.
//#define ShiftBit_Check_Enable //Check ShiftBit

//!Reset Code
#define RESET (NVIC_SystemReset()) //End loop go to Restart! ex( RESET;

//!Pin Settings
#define i2c_SDA (PB_7)
#define i2c_SCL (PB_6)
#define GP2Y_VPPpin (PA_12)

I2C i2c(i2c_SDA,i2c_SCL);
LCD lcd(i2c);
GP2Y_I2C gp2y(i2c,GP2Y_VPPpin);

Serial pc(USBTX,USBRX);
DigitalOut led(LED1);

DigitalIn SelectSW(PA_1);
DigitalIn  EnterSW(PA_0);

#ifndef AllTestMode
int main(void)
{
  int dis,old_dis;
  int TestAddress = 0x80;
  int WriteAddress= 0x00;
  bool GoWrite = 0;
  bool change = MenuEnable;
  bool MenuMode  = MenuEnable;
  bool WriteMode = WriteEnable;
  bool TestMode  = TestEnable + MenuMode;

  wait(2); //Serial Connect Wait...
  pc.printf("\n\n\nIgni...\n");
  lcd.locate(0,1);
  lcd.printf("igni...");

  //!MenuMode
  while(MenuMode)
  {
    if(change == 1)
    {
      pc.printf("Select -> ");
      lcd.printf("Select...");
      lcd.locate(0,1);
      if(TestMode==1)
      {
        pc.printf("TestMode");
        lcd.printf("TestMode");
      }
      else if(WriteMode==1)
      {
        pc.printf("AddressWrite");
        lcd.printf("AddWrite");
      }
      else TestMode = WriteMode = 0;
      pc.printf("\n");
      change = 0;
    }

    if(SelectSW == 0){
      WriteMode = TestMode;
      TestMode = !TestMode;
      if(WriteMode==0 && TestMode==0 || WriteMode==1 && TestMode == 1)
      {
        pc.printf("Sorry. Error now..._m_...\n");
        lcd.clear();
        lcd.locate(0,1);
        lcd.printf("Error.");
        TestMode=1;
        wait(2);
      }
      change = 1;
      wait(0.2);
    }
    else if(EnterSW == 0){
      wait(0.2);
      MenuMode = 0;
      break;
    }
    wait_ms(100);
  }
  //!MenuMode _END

  //!WriteMode
  if(WriteMode && !GoWrite)
  {
    pc.printf("\n <AddressWriteMode>\n Address Select -> SelectSwict.\n Write Select Address -> EnterSwitch.\n Cancel -> OnBoard Reset SW\n\n");
    change = 1;
    wait(1);
    while(1)
    {
      if(change == 1)
      {
        pc.printf("Address -> 0x%x",WriteAddress);
        lcd.reset();
        lcd.locate(0,1);
        lcd.printf("Ad->0x%x",WriteAddress);
        pc.printf("\n");
        change = 0;
      }
      if(SelectSW == 0)
      {
        if(WriteAddress == 0xF0) WriteAddress = 0x00;
        else WriteAddress += 0x10;
        change = 1;
      }
      else if(EnterSW == 0)
      {
        pc.printf("\n\n!WriteMode! You Select Address -> 0x%x\n Write -> EnterSW\n Cancel -> SelectSW\n" ,WriteAddress);
        lcd.reset();
        lcd.printf("OK>Enter");
        lcd.locate(0,1);
        lcd.printf("ad->0x%x" ,WriteAddress);
        wait(1);
        while(1)
        {
          wait(0.5);
          if(EnterSW == 0)
          {
            GoWrite = 1;
            break;
          }
          else if(SelectSW == 0)
          {
            wait(1);
            break;
          }
        }
      }
      wait_ms(100);
      if(GoWrite == 1) break;
    }
  }
  if(GoWrite)
  {
    WriteAddress = WriteAddress >> 4;
    pc.printf("\n\nWrite Sequence has wait 10 second.");
    pc.printf("\n\nLoading Write Sequence...\nYou Select Address -> 0x%x.\nConvert [WriteAddress >> 2 = SelectAd(0x%02x)]" ,WriteAddress<<4,WriteAddress);
    gp2y.GP2Y_ADDR_Write_Sequence(WriteAddress);
    wait(1);
    pc.printf("\n!!!Write Done!!!\n Please Hard_Reset!(Disconect Power Cable.)\n Enter for SoftReset.(Dont change address after Hard_Reset!)\n");
    lcd.reset();
    lcd.printf("Please");
    lcd.locate(0,1);
    lcd.printf("PowerOFF");
    while(1)
    {
      led = !led;
      if(EnterSW == 0) RESET;
      wait(1);
    }
  }
  //!WriteMode _END

  //!TestMode
  if(TestMode) pc.printf("\n <TestMode>\n ReBoot -> Please Push EnterSwitch.\n Test Address Change -> Push SelectSwitch.\n\n");
  while(TestMode)
  {
    dis = gp2y.Distance(TestAddress);
    if(old_dis != dis || change == 1)
    {
      lcd.reset();
      pc.printf("%2x -> %4d\n" ,TestAddress,dis);
      lcd.locate(0,1);
      lcd.printf("%2x->%4d" ,TestAddress,dis);
      change = 0;
    }
    old_dis = dis;
    if(SelectSW == 0)
    {
      if(TestAddress == 0xF0)TestAddress = 0x00;
      else TestAddress += 0x10;
      change = 1;
    }
    else if(EnterSW == 0) RESET;
    wait_ms(100);
  }
  //!TestMode _END

  //!ProgramEnd_AutoReset
  pc.printf("\n\n !Warning! Program End. Rebooting to after 5 sec...\n\n");
  lcd.clear();
  lcd.locate(0,1);
  lcd.printf("Res5Sec");
  wait(5);
  //NVIC_SystemReset();
  RESET;
  //!ProgramEnd_AutoReset _END
}
#endif

#ifdef AllTestMode
int main(void)
{
  int dis,a=0x00;
  wait(5);
  pc.printf("!!!Warning!!! This Mode Is Test Only. Not Main Function!\n");
  while(1)
  {
    pc.printf("\n\n");
    for(a=0x00;a<=0xF0;a+=0x10) pc.printf("|0x%2x" ,a);
    pc.printf("|\n");
    for(a=0x00;a<=0xF0;a+=0x10)
    {
      dis = gp2y.Distance(a);
      pc.printf("|%4d" ,dis);
      //if(a==0xf0) a=0xff;
    }
    #ifdef ShiftBit_Check_Enable
    pc.printf("|\n");
    for(a=0x00;a<=0xF0;a+=0x10)
    {
      pc.printf("|%4d" ,gp2y.ShiftBit_Check(a));
      //if(a==0xf0) a=0xff;
    }
    #endif
    pc.printf("|");
    wait_ms(500);
  }
}
#endif
