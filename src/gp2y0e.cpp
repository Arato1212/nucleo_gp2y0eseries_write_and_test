//2016/09/17

#include "gp2y0e.hpp"

#define GP2Y_WRITE_ADDR (0x80)

void GP2Y_I2C::i2c_write(int addr,char regist,char data)
{
    cmd[0]=regist;
    cmd[1]=data;
    _i2c.write(addr,cmd,2);
}

char GP2Y_I2C::i2c_read(int addr,char regist)
{
    cmd[0]=regist;
    _i2c.write(addr,cmd,1);
    _i2c.read(addr,cmd,1);
    return cmd[0];
}

/* Distance */
int GP2Y_I2C::Distance(int addr){
  CatchShiftBit = i2c_read(addr,ShiftBit_addr);
  return (int((i2c_read(addr,Distance_addr[0]))*16+(i2c_read(addr,Distance_addr[1]))/16/(CatchShiftBit*CatchShiftBit)));
}

/* ShiftBit Check */
int GP2Y_I2C::ShiftBit_Check(int addr)
{
  CatchShiftBit = i2c_read(addr,ShiftBit_addr);
  return (int(CatchShiftBit));
}

/**
 * @fn GP2Y Address Write Sequence
 * !!Warning!! "SelectAd" ... "0x0?". ex) Sensor Address 0x80 -> 0xA0. SelectAd=0x0A !Warning!
 **/
#ifdef GP2Y0E_I2C_ADDR_WRITE
void GP2Y_I2C::GP2Y_ADDR_Write_Sequence(char SelectAd){
  //wait 10 second
  for(int a=10 ; a>=0 ;a--){
    wait(1);
  }
  //Start!

  //Stage1
  i2c_write(GP2Y_WRITE_ADDR,0xEC,0xFF);
  wait_ms(1);

  //Writing Start!!
  _vpp = 1;
  wait(2); //wait...over 1 second

  //Stage2
  i2c_write(GP2Y_WRITE_ADDR,0xC8,0x00);
  wait_ms(1);

  //Stage3
  i2c_write(GP2Y_WRITE_ADDR,0xC9,0x45);
  wait_ms(1);

  //Stage4
  i2c_write(GP2Y_WRITE_ADDR,0xCD,SelectAd);
  wait_ms(1);

  //Stage5
  i2c_write(GP2Y_WRITE_ADDR,0xCA,0x01);
  wait_ms(1);
  wait(1);

  _vpp = 0;

  //Writing Done.

  wait(1);
}
#endif
