#include "i2clcd.hpp"
#include <mbed.h>
#include <cstdio>
#include <cstdarg>

#define ADDRESS             (0x7c)

#define POS2ADDRESS(c, r)   ((r*0x40)+(c))
#define DEFAULT_CONTRAST    (0x20)

#define TYPE_COMMAND        (0x00)
#define TYPE_DATA           (0x40)

#define CMD_FUNCTION_SET_NORMAL     (0x38)
#define CMD_FUNCTION_SET_EXTENDED   (0x39)
#define CMD_INTERNAL_OSC            (0x14)
#define CMD_CONTRAST1               (0x70)
#define CMD_CONTRAST2               (0x5c)
#define CMD_FOLLOWER_CTRL           (0x60)
#define CMD_DISPLAY_ON              (0x0c)
#define CMD_SET_CGRAM               (0x40)
#define CMD_RETURN_HOME             (0x02)
#define CMD_CLEAR_DISPLAY           (0x01)
#define CMD_ENTRY_MODE_SET          (0x04)

LCD::LCD(PinName sda, PinName scl) : _i2c(sda, scl) {
    _i2c.frequency(400*1000);
    reset();
}

LCD::LCD(I2C& i2c) : _i2c(i2c) {
    reset();
}

void LCD::reset() {
    wait(0.015);
    write_cmd(CMD_FUNCTION_SET_NORMAL);
    write_cmd(CMD_FUNCTION_SET_EXTENDED);
    write_cmd(CMD_INTERNAL_OSC);
    write_cmd(CMD_CONTRAST1 | (DEFAULT_CONTRAST & 0x0f));
    write_cmd(CMD_CONTRAST2 | ((DEFAULT_CONTRAST>>4) & 0x03));
    write_cmd(CMD_FOLLOWER_CTRL | 0x08/*Fon*/ | 0x04 /*Rab2*/);

    wait(0.3);
    write_cmd(CMD_FUNCTION_SET_NORMAL);
    write_cmd(CMD_DISPLAY_ON);

    clear();
}

void LCD::write(char data) {
    char cmd[2] = {TYPE_DATA, data};
    _i2c.write(ADDRESS, cmd, 2);
}

void LCD::write_cmd(char data) {
    char cmd[2] = {TYPE_COMMAND, data};
    _i2c.write(ADDRESS, cmd, 2);
}

LCD LCD::locate(int c, int r) {
    write_cmd(0x80 | POS2ADDRESS(c, r));
    return *this;
}

LCD LCD::home() {
    write_cmd(CMD_RETURN_HOME);
    wait(0.00164);
    return *this;
}

void LCD::clear() {
    write_cmd(CMD_CLEAR_DISPLAY); wait(0.00164);
    locate(0, 0);
}

void LCD::put(char ch) {
    write(ch);
}

void LCD::puts(const char *str) {
    while (*str) write(*str++);
}

#ifdef I2C_LCD_CONFIG_ENABLE_PRINTF
void LCD::printf(const char *fmt, ...) {
    char buf[32] = {'\0'};
    va_list args;
    va_start(args, fmt);
    vsprintf(buf, fmt, args);
    va_end(args);
    puts(buf);
}
#endif
#ifdef I2C_LCD_CONFIG_ENABLE_CONTRAST
void LCD::contrast(uint8_t contrast) {
    write_cmd(CMD_FUNCTION_SET_EXTENDED);
    write_cmd(CMD_CONTRAST1 | (contrast & 0x0f));
    write_cmd(CMD_CONTRAST2 | ((contrast>>4) & 0x03));
    write_cmd(CMD_FUNCTION_SET_NORMAL);
}
void LCD::resetContrast() {
    contrast(DEFAULT_CONTRAST);
}
#endif
